import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppMainComponent } from './layout/app-main/app-main.component';

const routes: Routes = [
  { path: '', redirectTo: 'app-main', pathMatch: 'full' },
  {
    path: '',
    component: AppMainComponent,
    children: [
      {
        path: '',
        loadChildren: () =>
          import('./layout/app-main/app-main.module').then(
            (m) => m.AppMainModule
          ),
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
