import { Injectable } from '@angular/core';
import { Profile } from './interfeces/profile';

@Injectable({
  providedIn: 'root',
})
export class LocalService {
  public profileInput: Profile[] = [];
  public textSearch: string = '';
  constructor() {}

  async setProfileInput(profileInput: Profile) {
    try {
      this.profileInput.push(profileInput);
      await localStorage.setItem(
        'profileInput',
        JSON.stringify(this.profileInput)
      );
    } catch (e) {
      console.log(e);
    }
  }

  setTextSearch(textSearch: string) {
    try {
      this.textSearch = textSearch;
    } catch (e) {
      console.log(e);
    }
  }

  async getProfileInput(): Promise<Profile[]> {
    try {
      const profileInput = await localStorage.getItem('profileInput');
      if (profileInput) {
        return JSON.parse(profileInput);
      } else {
        return [];
      }
    } catch (e) {
      console.log(e);
      return [];
    }
  }

  getTextSearch() {
    return this.textSearch;

  }
}
