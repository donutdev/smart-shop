export interface Profile {
  email: string;
  fistName: string;
  lastName: string;
  phone: string;
  address: string;
  district: string;
  province: string;
  zipCode: string;
}
