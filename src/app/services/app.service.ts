import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Profile } from './interfeces/profile';
import { LocalService } from './local.service';

@Injectable({
  providedIn: 'root',
})
export class AppService {
  constructor(private localService: LocalService, private router: Router) {}

  async getProfileInput(): Promise<Profile[]> {
    try {
      const profileInput =  localStorage.getItem('profileInput');
      if (profileInput) {
        this.router.navigate(['/basket']);
        return JSON.parse(profileInput);
      } else {
        this.router.navigate(['/profile']);
        return [];
      }
    } catch (e) {
      console.log(e);
      return [];
    }
  }
}
