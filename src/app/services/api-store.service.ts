import { Injectable } from '@angular/core';

import { HttpClient } from '@angular/common/http';

import { ResStoreAll } from './interfeces/fakeStore';
import { Observable } from 'rxjs';
import { environment } from '../environments/environment.prod';

@Injectable({
  providedIn: 'root',
})
export class ApiStoreService {
  constructor(private http: HttpClient) {}


  getFindAll(): Observable<ResStoreAll[]> {
    return this.http.get<ResStoreAll[]>(`${environment.Url}/products`, {});
  }

  getFindById(id: number): Observable<ResStoreAll> {
    return this.http.get<ResStoreAll>(`${environment.Url}/products/${id}`);
  }
}
