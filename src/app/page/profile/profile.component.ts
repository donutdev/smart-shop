import { Component, OnInit } from '@angular/core';
import { Profile } from 'src/app/services/interfeces/profile';
import { LocalService } from 'src/app/services/local.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css'],
})
export class ProfileComponent implements OnInit {
  constructor(private localService: LocalService) {}

  ngOnInit() {}
}
