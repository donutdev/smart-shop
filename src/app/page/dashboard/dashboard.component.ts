import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiStoreService } from 'src/app/services/api-store.service';
import { ResStoreAll } from 'src/app/services/interfeces/fakeStore';
import { LocalService } from 'src/app/services/local.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css'],
})
export class DashboardComponent implements OnInit {
  public listAll: ResStoreAll[] = [];
  public textSearch = '';

  constructor(
    private apiStoreService: ApiStoreService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.getFindAll();
  }

  getFindAll() {
    this.apiStoreService.getFindAll().subscribe(
      (res) => {
        this.listAll = res;
        console.log('res', res);
      },
      (err) => {
        console.log(err);
      }
    );
  }

  async chooseProduct(id: number) {
    await this.router.navigate(['/detail'], { queryParams: { id: id } });
  }

  search() {
    console.log('textSearch', this.textSearch);
    if (this.textSearch === '') {
      this.getFindAll();
      this.textSearch = '';
      return;
    } else {
      this.listAll = this.listAll.filter((item) => {
        return item.title.toLowerCase().includes(this.textSearch.toLowerCase());
      });
    }
  }
}
