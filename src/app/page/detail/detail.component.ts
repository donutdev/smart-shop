import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiStoreService } from 'src/app/services/api-store.service';
import { AppService } from 'src/app/services/app.service';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.css'],
})
export class DetailComponent implements OnInit {
  public datas: any = [];
  constructor(
    private activatedRoute: ActivatedRoute,
    private apiStoreService: ApiStoreService
  ) {}

  async ngOnInit() {
    this.getData();
  }

  getData() {
    this.activatedRoute.queryParams.subscribe(async (params) => {
      const data = params['id'];
      console.log('data', data);
      this.apiStoreService.getFindById(data).subscribe(
        (data) => {
          if (data) {
            console.log('data', data);
            this.datas.push(data);
          }
        },
        (error) => {
          console.log('error', error);
        }
      );
    });
  }
}
