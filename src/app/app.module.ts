import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DashboardComponent } from './page/dashboard/dashboard.component';
import { ProfileComponent } from './page/profile/profile.component';
import { FormsModule } from '@angular/forms';
import { DetailComponent } from './page/detail/detail.component';
import { BasketComponent } from './page/basket/basket.component';

@NgModule({
  declarations: [AppComponent, DashboardComponent, ProfileComponent, DetailComponent, BasketComponent],
  imports: [BrowserModule, AppRoutingModule, HttpClientModule, FormsModule],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
