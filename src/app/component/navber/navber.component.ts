import { Component, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import * as moment from 'moment';
import { LocalService } from 'src/app/services/local.service';

@Component({
  selector: 'app-navber',
  templateUrl: './navber.component.html',
  styleUrls: ['./navber.component.css'],
})
export class navbarComponent implements OnInit {
  public category = [];
  public textSearch = '';
  public time = '';
  constructor(private localService: LocalService, private router: Router) {}

  ngOnInit(): void {
    this.timer();
  }

  timer() {
    setInterval(() => {
      this.time = moment().format('LTS');
    }, 1000);
  }
}
