import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AppMainRoutingModule } from './app-main-routing.module';
import { AppMainComponent } from './app-main.component';
import { navbarComponent } from 'src/app/component/navber/navber.component';
import { FootbarComponent } from 'src/app/component/footbar/footbar.component';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [AppMainComponent, navbarComponent, FootbarComponent],
  imports: [CommonModule, AppMainRoutingModule, FormsModule],
})
export class AppMainModule {}
