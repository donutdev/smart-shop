import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BasketComponent } from 'src/app/page/basket/basket.component';
import { DashboardComponent } from 'src/app/page/dashboard/dashboard.component';
import { DetailComponent } from 'src/app/page/detail/detail.component';
import { ProfileComponent } from 'src/app/page/profile/profile.component';
import { AppMainComponent } from './app-main.component';

const routes: Routes = [
  {
    path: '',
    component: AppMainComponent,
    children: [
      { path: '', component: DashboardComponent },
      { path: 'dashboard', component: DashboardComponent },
      { path: 'profile', component: ProfileComponent },
      { path: 'detail', component: DetailComponent },
      { path: 'basket', component: BasketComponent },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AppMainRoutingModule {}
